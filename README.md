# README #

# Bakuemlak project #

### --- Linux da run elemek ucun ###

```sh
$ git clone https://bitbucket.org/munis/bakuelan.com
$ cd bakuelan.com/
$ python3 -m venv .venv
$ source .venv/bin/activate/
(.venv)$ pip install -r requirements.txt
(.venv)$ ./manage.py migrate
(.venv)$ ./manage.py runserver 
```

### Windows da run elemek ucun ###
Evvelce git bash i yuklemek lazimdir [**Git bash for windows.**](https://git-scm.com/downloads)<br>
Sonra python3 u yuklemek lazimdi ve Enviroment Varibles da qeydiyyatdan kecirmek lazimdi [**Python3 installation windows**](https://www.youtube.com/watch?v=V_ACbv4329E)
En sonda desktop da papka yaradib proyekti clone elemek lazimdi
sonrada ashagidaki emrleri icra elemek.
```sh
$ git clone https://bitbucket.org/munis/bakuelan.com
$ cd bakuelan.com/
$ python3 -m venv .venv
$ cd .venv/bin/
$ activate.bat
(.venv)$ pip install -r requirements.txt
(.venv)$ python manage.py migrate
(.venv)$ python manage.py runserver 
```

