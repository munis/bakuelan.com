from django.conf.urls import url
#from django.contrib import sitemaps
from django.contrib.auth import views as auth_views
#from django.contrib.sitemaps.views import sitemap
from . import views
from news.views import *

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^search/$', views.Search , name='search'),
    url(r'^detail/(?P<article_id>[0-9]+)/(.+?)/$', views.detail, name='detail'),
    url(r'^blog/(?P<blog_id>[0-9]+)/$', views.blog_single, name='detail_blog'),
    url(r'^qeydiyyat/$', Register.as_view(), name='qeydiyyat'),
    url(r'^girish/$', login_view, name="login"),
    url(r'^cixish/$', auth_views.logout, name="logout",  kwargs={'next_page': '/'}),
    url(r'^contact/$', Contact_Us.as_view(), name="contact"),
    url(r'^contacts/$', views.contacts, name="contacts"),
    url(r'^test/$', views.new_temp, name="elan"),
    url(r'^article_yaz/$', WriteArticle.as_view(), name="meqale"),
    url(r'^invalid/$', views.invalid, name="invalid"),
    url(r'^formsehvdir/$', views.invalid, name="invalid"),
    url(r'^error/$', views.invalids, name="invalids"),
    url(r'^elan_elave_et/$', views.add_elan, name="add_elan"),
    url(r'^vip_elan/$', views.vip_elan, name="vip_elan"),
    url(r'^butun_elanlar/$', views.all_elan, name="butun"),
    url(r'^blogs/$', views.blogs, name="blogs"),
    url(r'^about/$', views.About, name="about"),
    #url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    url(r'^user/$', views.user_single, name="user"),
    url(r'^developer/$', views.developer, name="developer"),
    url(r'^inspection/$', views.inspection, name="istifade_shertleri"),
]
