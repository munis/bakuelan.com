from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.forms import Textarea


class RegisterForm(forms.Form):
    parol = forms.CharField(max_length=100)
    parolun_tesdiqi = forms.CharField(max_length=100)
    adi = forms.CharField(max_length=255)
    soyadi = forms.CharField(max_length=255)
    e_mail = forms.EmailField()
    tel = forms.CharField(max_length=20)
    cins = forms.IntegerField()


class LoginForm(forms.Form):
    e_mail = forms.EmailField()
    shifre = forms.CharField(max_length=100)



class AddArticleForm(forms.Form):
    """ Elan elave etmek formu """
    docfile = forms.ImageField()
    basliq = forms.CharField(max_length=100)
    content = forms.CharField(widget=Textarea)
    erazi = forms.IntegerField()
    kend = forms.CharField(max_length=100)
    qiymet = forms.CharField()
    shekil_1 = forms.ImageField()
    shekil_2 = forms.ImageField()
    shekil_3 = forms.ImageField()
    shekil_4 = forms.ImageField()
    mertebe = forms.CharField(max_length=20)
    otaq = forms.CharField(max_length=20)
    qaraj = forms.CharField(max_length=20)


class SearchForm(forms.Form):
    basliq = forms.CharField(max_length=40)
    erazi = forms.IntegerField()
    tipi = forms.IntegerField()
    max_prize = forms.CharField()
    min_prize = forms.CharField()


class ContactForm(forms.Form):
    full_name = forms.CharField(max_length=100)
    email = forms.EmailField()
    phone = forms.IntegerField()
    website = forms.CharField()
    content = forms.CharField(widget=Textarea)
