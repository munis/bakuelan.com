# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-24 13:22
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_article'),
    ]

    operations = [
        migrations.CreateModel(
            name='Images',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cover_photo', models.ImageField(blank=True, null=True, upload_to='images/albums/')),
                ('images_field_1', models.ImageField(blank=True, null=True, upload_to='images/albums/')),
                ('images_field_2', models.ImageField(blank=True, null=True, upload_to='images/albums/')),
                ('images_field_3', models.ImageField(blank=True, null=True, upload_to='images/albums/')),
                ('images_field_4', models.ImageField(blank=True, null=True, upload_to='images/albums/')),
            ],
        ),
        migrations.RemoveField(
            model_name='article',
            name='cover_photo',
        ),
        migrations.RemoveField(
            model_name='article',
            name='images_field_1',
        ),
        migrations.RemoveField(
            model_name='article',
            name='images_field_2',
        ),
        migrations.RemoveField(
            model_name='article',
            name='images_field_3',
        ),
        migrations.RemoveField(
            model_name='article',
            name='images_field_4',
        ),
        migrations.AddField(
            model_name='article',
            name='article_images',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='news.Images'),
            preserve_default=False,
        ),
    ]
