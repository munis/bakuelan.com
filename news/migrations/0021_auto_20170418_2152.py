# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2017-04-18 21:52
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import embed_video.fields


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0020_auto_20160504_2101'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='permission',
            field=models.BooleanField(default=False, verbose_name='Admin icazesi'),
        ),
        migrations.AddField(
            model_name='article',
            name='video',
            field=embed_video.fields.EmbedVideoField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='article',
            name='created',
            field=models.DateTimeField(default=django.utils.timezone.now, editable=False),
        ),
        migrations.AlterField(
            model_name='blog',
            name='datatime',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='contact',
            name='datatime',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
