# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-01 13:30
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0014_auto_20160501_1830'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 1, 13, 30, 47, 99865, tzinfo=utc), editable=False),
        ),
        migrations.AlterField(
            model_name='blog',
            name='datatime',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 1, 18, 30, 47, 100866)),
        ),
        migrations.AlterField(
            model_name='contact',
            name='datatime',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 1, 18, 30, 47, 101867)),
        ),
    ]
