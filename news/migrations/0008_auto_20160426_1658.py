# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-26 11:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0007_auto_20160425_2329'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='article',
            options={'ordering': ('-header',), 'verbose_name': 'Meqale', 'verbose_name_plural': 'Meqaleler'},
        ),
        migrations.AddField(
            model_name='article',
            name='slide',
            field=models.BooleanField(default=False, verbose_name='Slayder'),
        ),
        migrations.AddField(
            model_name='article',
            name='vip_elan',
            field=models.BooleanField(default=False, verbose_name='Vip Elan'),
        ),
        migrations.AlterField(
            model_name='article',
            name='garage',
            field=models.CharField(max_length=2),
        ),
    ]
