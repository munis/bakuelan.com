from django.shortcuts import render_to_response, get_object_or_404, render, redirect
from django.core.context_processors import csrf
from django.views.generic import View
from django.db.utils import IntegrityError
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.contrib import messages
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseNotAllowed, Http404, HttpResponseForbidden
from news.models import *
from news.forms import RegisterForm, LoginForm, AddArticleForm , SearchForm, ContactForm
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from django.db.models import Q
from django.db.models import Avg, Max, Min
from django.core.mail import send_mail

import json



# This is the main objects wich can use all pages

def base(req=None):
    data = {
        "news_index":Article.objects.filter(slide=True,permission=True),
        "vip_article":Article.objects.filter(vip_elan=True,permission=True),

    }
    data.update(csrf(req))
    return data



# This is main page views

def index(request):
    rend_it = base(req=request)
    rend_it["users"] = UserProfile.objects.all()[:4]
    rend_it["all_article"] = Article.objects.filter(permission=True)[:9]
    rend_it["blogs"] = Blog.objects.all()[:4]
    return render_to_response("index.html", rend_it, context_instance=RequestContext(request))


# about views

def About(request):
    rend_it = base(req=request)
    return render_to_response('about.html',rend_it, context_instance=RequestContext(request))



# class Search(View):
#     def post(self,request):
#         rend_it = base(req=request)
#         post = request.POST.copy()
#         form = SearchForm(post)
#         if form.is_valid() or request.POST['basliq'] == '':
#             data = form.cleaned_data
#             if data.get('basliq') is None:lear
#                 rend_it['seen'] = Article.objects.filter(area=data.get('erazi'),types=data.get('tipi'),prize=data.get(''))
#                 return render_to_response('search.html',rend_it ,context_instance=RequestContext(request))
#             else:
#                 rend_it['seen'] = Article.objects.filter(header__icontains=data.get('basliq'),area=data.get('erazi'),)
#                 return render_to_response('search.html',rend_it ,context_instance=RequestContext(request))
#         else:
#             return redirect('/')



def new_temp(request):
    rend_it = base(req=request)
    return render_to_response('test.html',rend_it, context_instance=RequestContext(request))




def Search(request):
    if request.method == 'GET' and 'erazi' in request.GET:
        rend_it = base(req=request)
        get = request.GET
        form = SearchForm(get)
        if form.is_valid() or get['basliq'] == '' or get['max_prize'] == '':
            data = form.cleaned_data
            if request.GET['basliq'] == '':
                if request.GET['min_prize'] == '' and request.GET['max_prize'] == '':
                    rend_it['seen'] = Article.objects.filter(permission=True,area=data['erazi'],types=data['tipi'])
                    return render_to_response('search.html',rend_it ,context_instance=RequestContext(request))
                elif request.GET['max_prize'] == '':
                    rend_it['seen'] = Article.objects.filter(permission=True,area=data['erazi'],types=data['tipi'],prize__range=[data['min_prize'],500000])
                    return render_to_response('search.html',rend_it ,context_instance=RequestContext(request))
                elif request.GET['min_prize'] == '':
                    rend_it['seen'] = Article.objects.filter(permission=True,area=data['erazi'],types=data['tipi'],prize__range=[0,data['max_prize']])
                    return render_to_response('search.html',rend_it ,context_instance=RequestContext(request))
                else:
                    rend_it['seen'] = Article.objects.filter(permission=True,area=data['erazi'],types=data['tipi'],prize__range=[data['min_prize'],data['max_prize']])
                    return render_to_response('search.html',rend_it ,context_instance=RequestContext(request))


            else:
                if request.GET['min_prize'] == '' and request.GET['max_prize'] == '':
                    rend_it['seen'] = Article.objects.filter(permission=True,header__contains=data['basliq'],area=data['erazi'],types=data['tipi'])
                    return render_to_response('search.html',rend_it ,context_instance=RequestContext(request))
                elif request.GET['max_prize'] == '':
                    rend_it['seen'] = Article.objects.filter(permission=True,header__contains=data['basliq'],area=data['erazi'],types=data['tipi'],prize__range=[data['min_prize'],500000])
                    return render_to_response('search.html',rend_it ,context_instance=RequestContext(request))
                elif request.GET['min_prize'] == '':
                    rend_it['seen'] = Article.objects.filter(permission=True,header__contains=data['basliq'],area=data['erazi'],types=data['tipi'],prize__range=[0,data['max_prize']])
                    return render_to_response('search.html',rend_it ,context_instance=RequestContext(request))
                else:
                    rend_it['seen'] = Article.objects.filter(permission=True,header__contains=data['basliq'],area=data['erazi'],types=data['tipi'],prize__range=[data['min_prize'],data['max_prize']])
                    return render_to_response('search.html',rend_it ,context_instance=RequestContext(request))

        else:
            return redirect('/formvaliddeyil')

    else:
        rend_it = base(req=request)
        return render_to_response('search.html',rend_it ,context_instance=RequestContext(request))
        # rend_it = base(req=request)
        # query = request.POST
        # if 'basliq' in query or 'erazi' in query:
        #     post = request.POST.copy()
        #     form = SearchForm(post)
        #     if form.is_valid:
        #         form.cleaned_data
        #         rend_it['seen'] = Article.objects.filter(header__icontains='a')
        #         return render_to_response('search.html',rend_it, context_instance=RequestContext(request))
        #     else:
        #         return render_to_response('search.html', rend_it, context_instance=RequestContext(request))
        # else:
        #     return render_to_response('search.html', rend_it, context_instance=RequestContext(request))





def invalid(request):
    rend_it = base(req=request)
    return render_to_response('invalid.html', rend_it, context_instance=RequestContext(request))



def invalids(request):
    rend_it = base(req=request)
    return render_to_response('form_invalid.html', rend_it, context_instance=RequestContext(request))

def detail(request,article_id):
    rend_it = base(req=request)
    rend_it['article_index'] = get_object_or_404(Article, pk=article_id)
    rend_it['article_image'] = Article_Images.objects.all()[:4]
    z = list(Article.objects.filter(permission=True))
    b = list(Article.objects.filter(vip_elan=True))
    c = list(Blog.objects.all())
    rend_it['all_num'] = len(z)
    rend_it['vip_num'] = len(b)
    rend_it['blog_num'] = len(c)
    return render_to_response('detail.html', rend_it, context_instance=RequestContext(request))


def vip_elan(request):
    rend_it = base(req=request)
    rend_it['detail_vip'] = Article.objects.filter(vip_elan=True)
    return render_to_response('vip_elan.html', rend_it, context_instance=RequestContext(request))


def all_elan(request):
    rend_it = base(req=request)
    rend_it['all_elan'] = Article.objects.filter(permission=True)
    return render_to_response('all_article.html', rend_it, context_instance=RequestContext(request))

def blogs(request):
    rend_it = base(req=request)
    rend_it['all_blog'] = Blog.objects.all()
    return render_to_response('blogs.html', rend_it, context_instance=RequestContext(request))


def blog_single(request,blog_id):
    rend_it = base(req=request)
    rend_it['single_blog'] = get_object_or_404(Blog,pk=blog_id)
    z = list(Article.objects.filter(permission=True))
    b = list(Article.objects.filter(vip_elan=True))
    c = list(Blog.objects.all())
    rend_it['all_num'] = len(z)
    rend_it['vip_num'] = len(b)
    rend_it['blog_num'] = len(c)
    return render_to_response('blog-single.html', rend_it, context_instance=RequestContext(request))

def user_single(request):
    current_user = request.user
    if current_user.is_active:
        rend_it = base(req=request)
        rend_it['user_name'] = UserProfile.objects.filter(user_id=current_user.id)[0]
        rend_it['user_info'] = Article.objects.filter(author_id=current_user.id)
        return render_to_response('user-single.html', rend_it, context_instance=RequestContext(request))
    else:
        return Http404()


def developer(request):
    rend_it = base(req=request)
    return render_to_response("admin.html", rend_it, context_instance=RequestContext(request))

def inspection(request):
    rend_it = base(req=request)
    return render_to_response("useful.html", rend_it, context_instance=RequestContext(request))



class Register(View):
    def post(self, request):
        post = request.POST.copy()
        form = RegisterForm(post)
        if form.is_valid() and post['parol'] == post['parolun_tesdiqi']:
            data = form.cleaned_data
            try:
                user = User.objects.create_user(
                    username=data.get("e_mail"),
                    email=data.get("e_mail"),
                    password=data.get("parol"),
                    first_name=data.get("adi"),
                    last_name=data.get("soyadi")
                )

                UserProfile.objects.create(
                    user=user,
                    phone=data.get("tel"),
                    gender=data.get("cins")
                )
                messages.success(
                    request,
                    _('Təşəkkür edirik. Qeydiyyatınız uğurla tamamlandı, giriş edə bilərsiniz.')
                )
                return redirect('/')
            except IntegrityError:
                pass
        else:
            return redirect(reverse('invalids'))


def login_view(request):
    if request.method == 'POST':
        post = request.POST.copy()
        form = LoginForm(post)
        if form.is_valid():
            data = form.cleaned_data
            user = authenticate(username=data.get("e_mail"), password=data.get("shifre"))
            if user:
                if user.is_active:
                    login(request, user)
                    return redirect(reverse('index'))
                else:
                    raise Http404()
            else:
                return redirect(reverse('invalid'))
        else:
            return redirect(reverse('invalid'))
    else:
        return HttpResponseNotAllowed()



class WriteArticle(View):
    def post(self, request):
        if request.method == 'POST':
            post = request.POST.copy()
            current_user = request.user
            form = AddArticleForm(post,request.FILES)
            if form.is_valid() or request.POST['kend2'] == '':
                data = form.cleaned_data
                try:
                    a = Article_Images(images=request.FILES['docfile'],detail_1=request.FILES['shekil_1'],detail_2=request.FILES['shekil_2'],detail_3=request.FILES['shekil_3'],detail_4=request.FILES['shekil_4'])
                    a.save()
                    b = Article(
                        article_images_id=a.id,
                        header=data.get("basliq"),
                        village=data.get("kend1"),
                        about=data.get("content"),
                        area=data.get("erazi"),
                        prize=data.get("qiymet"),
                        floor=data.get("mertebe"),
                        author_id=current_user.id,
                        rooms=data.get("otaq"),
                        garage=data.get("qaraj")
                    )
                    b.save()

                    messages.success(
                        request,
                        _('Təşəkkür edirik. Elanınız uğurla yerləşdirildi, Ana səhifədə günün elanı bölməsində öz elanınızı görə bilərsiniz')
                    )
                    return redirect('/')
                except:
                    return redirect('/invalid')
            elif form.is_valid() or request.POST['kend1'] == '':
                data = form.cleaned_data
                try:
                    a = Article_Images(images=request.FILES['docfile'],detail_1=request.FILES['shekil_1'],detail_2=request.FILES['shekil_2'],detail_3=request.FILES['shekil_3'],detail_4=request.FILES['shekil_4'])
                    a.save()
                    b = Article(
                        article_images_id=a.id,
                        header=data.get("basliq"),
                        village=data.get("kend2"),
                        about=data.get("content"),
                        area=data.get("erazi"),
                        prize=data.get("qiymet"),
                        floor=data.get("mertebe"),
                        author_id=current_user.id,
                        rooms=data.get("otaq"),
                        garage=data.get("qaraj")
                    )
                    b.save()

                    messages.success(
                        request,
                        _('Təşəkkür edirik. Elanınız uğurla yerləşdirildi, Ana səhifədə günün elanı bölməsində öz elanınızı görə bilərsiniz')
                    )
                    return redirect('/')
                except:
                    return redirect('/invalid')
            else:
                return HttpResponseForbidden()
        else:
            return HttpResponseBadRequest()





def add_article(request):
    rend_it = base(req=request)
    return render_to_response('test.html',rend_it,context_instance=RequestContext(request))

def add_elan(request):
    rend_it = base(req=request)
    current_user = request.user
    if current_user.is_active:
        return render_to_response('elan.html',rend_it,context_instance=RequestContext(request))
    else:
        return render_to_response('invalid.html',rend_it,context_instance=RequestContext(request))


def contacts(request):
    rend_it = base(req=request)
    return render_to_response('contact.html',rend_it,context_instance=RequestContext(request))



class Contact_Us(View):
    def post(self, request):
        if request.method == 'POST':
            post = request.POST.copy()
            form = ContactForm(post)
            if form.is_valid() or request.POST['website'] == '':
                data = form.cleaned_data
                try:
                    send_mail(data.get("full_name"),data.get("content") ,data.get("email"),
    ['munisisazade@gmail.com'], fail_silently=False)
                    b = Contact(
                        name=data.get("full_name"),
                        email=data.get("email"),
                        phone=data.get("phone"),
                        website=data.get("website"),
                        content=data.get("content")
                    )
                    b.save()

                    messages.success(
                        request,
                        _('Təşəkkür edirik. Mesajınız uğurla göndərildi ')
                    )

                    return redirect('/')
                except:
                    return redirect('/invalid')
            else:
                return HttpResponseForbidden()
        else:
            return HttpResponseBadRequest()
# sitemap
def sitemap(request):
    rend_it = base(req=request)
    return render_to_response('sitemap.xml',rend_it,context_instance=RequestContext(request))
