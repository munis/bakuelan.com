from PIL import Image
import os
from django.conf import settings





def image_resize(img_path, newwidth, newheight):
	try:
		img = Image.open(img_path)
		img.thumbnail([newwidth, newheight],Image.ANTIALIAS)
		img.save(img_path,quality=75)
	except FileNotFoundError:
		print('File not found')


