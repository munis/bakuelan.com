from django.db import models
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField
from django.utils import timezone
import datetime
from PIL import Image as Img
import os
from django.conf import settings
from news.helper import image_resize
from embed_video.fields import EmbedVideoField
from django.core.urlresolvers import reverse
# Create your models here.




class UserProfile(models.Model):
    user = models.OneToOneField(User, verbose_name="İstifadəçi")

    GENDER_CHOISE = (
        (1, 'Kişi'),
        (2, 'Qadın')
    )
    gender = models.IntegerField(choices=GENDER_CHOISE, verbose_name="Cinsi")
    phone = models.CharField(max_length=20, verbose_name="Telefon nömrəsi")

    class Meta:
        verbose_name = "İstifadəçi"
        verbose_name_plural = "İstifadəçilər"

    def __str__(self):
        return "%s %s" % (self.user.first_name, self.user.last_name)



class Article_Images(models.Model):
    """ Elanin butun shekilleri """
    images = models.ImageField(upload_to='images/albums/',blank=True,null=True)
    detail_1 = models.ImageField(upload_to='images/albums/',blank=True,null=True)
    detail_2 = models.ImageField(upload_to='images/albums/',blank=True,null=True)
    detail_3 = models.ImageField(upload_to='images/albums/',blank=True,null=True)
    detail_4 = models.ImageField(upload_to='images/albums/',blank=True,null=True)
    class Meta:
        verbose_name = "Şəkil"
        verbose_name_plural = "Şəkillər"

    def __str__(self):
        return self.images.name[14:]
    def save(self, *args,**kwargs):
        super(Article_Images, self).save(*args, **kwargs)
        image_path = os.path.join(settings.MEDIA_ROOT, self.images.path)
        image_path_1 = os.path.join(settings.MEDIA_ROOT, self.detail_1.path)
        image_path_2 = os.path.join(settings.MEDIA_ROOT, self.detail_2.path)
        image_path_3 = os.path.join(settings.MEDIA_ROOT, self.detail_3.path)
        image_path_4 = os.path.join(settings.MEDIA_ROOT, self.detail_4.path)
        image_resize(image_path, 880, 443)
        image_resize(image_path_1, 880, 443)
        image_resize(image_path_2, 880, 443)
        image_resize(image_path_3, 880, 443)
        image_resize(image_path_4, 880, 443)








class Article(models.Model):
    """ Bu classda elanin databesini teshkil edir """
    header = models.CharField(max_length=100)
    about = RichTextField(config_name='awesome_ckeditor')
    area_text = (
        (1,'Bakı'),
        (2,'Abşeron')
    )
    area = models.IntegerField(choices=area_text, verbose_name="Erazi", default='1')
    village = models.CharField(max_length=100,verbose_name="Erazi",blank=True, null=True)
    prize = models.IntegerField()
    dimention = models.IntegerField(default=120)
    article_images = models.ForeignKey(Article_Images,blank=True, null=True)
    type_sale = 1
    type_rent = 2
    types_text = (
        (type_sale,'Satış'),
        (type_rent,'İcarə')
    )
    types = models.IntegerField(choices=types_text, verbose_name="Tipi", default='1')
    author = models.ForeignKey(User)
    video = EmbedVideoField(null=True,blank=True)
    category_text = (
        (1,'Navastroyka'),
        (2,'Bina'),
        (3,'Həyət evi')
    )
    category = models.IntegerField(choices=category_text, verbose_name="Kateqoriyasi", default='1')
    floor = models.CharField(max_length=20)
    rooms = models.CharField(max_length=20)
    garage = models.CharField(max_length=2)
    permission = models.BooleanField(default=False, verbose_name="Admin icazesi")
    slide = models.BooleanField(default=False,verbose_name="Slayder")
    vip_elan = models.BooleanField(default=False,verbose_name="Vip Elan")
    created = models.DateTimeField(editable=False,default=timezone.now)
    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created = timezone.now()
        return super(Article, self).save(*args, **kwargs)

    class Meta:

        verbose_name = "Meqale"
        verbose_name_plural = "Meqaleler"
        ordering = ('-created',)

    def __str__(self):
        return self.header

    def slugger(self):
        return self.header.replace(" ","_")

    def get_absolute_url(self):
        return reverse('detail', args=[self.id,self.header.replace(" ","_")])


class Blog(models.Model):
    image = models.ImageField(upload_to='images/albums/',blank=True,null=True)
    header = models.CharField(max_length=100)
    content = RichTextField(config_name='awesome_ckeditor')
    datatime = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User)



    class Meta:
        verbose_name = "Blog"
        verbose_name_plural = "Bloglar"
        ordering = ('-datatime',)

    def __str__(self):
        return self.header

    def get_absolute_url(self):
        return reverse('detail_blog', args=[self.id])



class Contact(models.Model):
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    phone = models.IntegerField()
    website = models.CharField(max_length=100,blank=True,null=True)
    content = RichTextField(config_name='awesome_ckeditor')
    datatime = models.DateTimeField(default=timezone.now)
    class Meta:
        verbose_name = "Kontakt"
        verbose_name_plural = "Mesajlar"
        ordering = ('-datatime',)

    def __str__(self):
        return self.name
