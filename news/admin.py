from django.contrib import admin
from news.models import *
# Register your models here.

admin.site.register(UserProfile)
admin.site.register(Article)
admin.site.register(Article_Images)
admin.site.register(Blog)
admin.site.register(Contact)


class PropertyImageInline(admin.TabularInline):
    model = Article_Images
    extra = 3

class PropertyAdmin(admin.ModelAdmin):
    inlines = [PropertyImageInline,]