from django.contrib import sitemaps
from django.core.urlresolvers import reverse
from news.models import *


class StaticViewSitemap(sitemaps.Sitemap):
    priority = 0.5
    changefreq = 'daily'

    def items(self):
        return Article.objects.all()

    def lastmod(self, obj):
        return obj.created
    def location(self,item):
    	return Article.get_absolute_url(item)

class BlogViewSitemap(sitemaps.Sitemap):
 	priority = 0.5
 	changefreq = 'daily'

 	def items(self):
 		return Blog.objects.all()

 	def lastmod(self, obj):
 		return obj.datatime

 	def location(self,item):
 		return Blog.get_absolute_url(item)


class Permission(sitemaps.Sitemap):
	priority = 0.5
	changefreq = 'daily'

	
	def location(self,item):
		return reverse('istifade_shertleri')